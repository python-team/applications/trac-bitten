<!DOCTYPE html>

<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="generator" content="Docutils 0.5: http://docutils.sourceforge.net/">
<title>Bitten: Configuration</title>
<link rel="stylesheet" href="common/style/edgewall.css" type="text/css">
</head>
<body>
<div class="document" id="configuration">
    <div id="navigation">
      <span class="projinfo">Bitten 0.6</span>
      <a href="index.html">Documentation Index</a>
    </div>
<h1 class="title">Configuration</h1>
<p>While the <a class="reference external" href="recipe.html">Recipe</a> contains the instructions for how to build,
configurations are used to determine what gets built and any runtime
parameters used when building.</p>
<div class="contents topic" id="contents">
<p class="topic-title first">Contents</p>
<ul class="auto-toc simple">
<li><a class="reference internal" href="#target-platforms" id="id1">1   Target platforms</a></li>
<li><a class="reference internal" href="#slave-properties" id="id2">2   Slave Properties</a><ul class="auto-toc">
<li><a class="reference internal" href="#examples" id="id3">2.1   Examples</a></li>
</ul>
</li>
<li><a class="reference internal" href="#slave-configuration" id="id4">3   Slave Configuration</a><ul class="auto-toc">
<li><a class="reference internal" href="#configuration-file-format" id="id5">3.1   Configuration File Format</a></li>
<li><a class="reference internal" href="#commands-using-properties" id="id6">3.2   Commands using Properties</a></li>
<li><a class="reference internal" href="#properties-in-build-configurations" id="id7">3.3   Properties in Build Configurations</a></li>
<li><a class="reference internal" href="#property-interpolation-in-build-recipes" id="id8">3.4   Property Interpolation in Build Recipes</a></li>
</ul>
</li>
<li><a class="reference internal" href="#authentication" id="id9">4   Authentication</a></li>
</ul>
</div>
<div class="section" id="target-platforms">
<h1>1   Target platforms</h1>
<p>A target platform is something like 'NetBSD x86' or 'Win32 Java 1.4'.</p>
<p>Technically, a target platform is a named set of rules against which the
properties of build slaves are matched. Each rule is a regular expression
matching a particular slave property, such as the operating system
or the processor. When a slave connects to the build master, it sends a
registration message that includes information about the slave. A slave
will only be sent builds for a given platform if the slave's properties
satisfy all of the rules associated with that platform.</p>
<p>A rule's regular expression is matched against the value of the slave
property using Python's <a class="reference external" href="http://docs.python.org/library/re.html#re.match">re.match</a> function so, for example, <cite>x86</cite> will
match a value of <cite>x86_64</cite>. Use <cite>^x86$</cite> to match only the value <cite>x86</cite>.</p>
<p>A build configuration must have at least one target platform assigned to
it before it becomes fully active.</p>
</div>
<div class="section" id="slave-properties">
<h1>2   Slave Properties</h1>
<p>By default, the following properties are included:</p>
<table class="docutils field-list" frame="void" rules="none">
<col class="field-name">
<col class="field-body">
<tbody valign="top">
<tr class="field"><th class="field-name"><cite>family</cite>:</th><td class="field-body">The basic type of operating system, typically “posix” for
Unix-like systems and “nt” for Win32 systems.</td>
</tr>
<tr class="field"><th class="field-name"><cite>os</cite>:</th><td class="field-body">The name of the operating system (for example “Darwin”,
“Linux” or “Windows”).</td>
</tr>
<tr class="field"><th class="field-name"><cite>version</cite>:</th><td class="field-body">The operating system version.</td>
</tr>
<tr class="field"><th class="field-name"><cite>machine</cite>:</th><td class="field-body">The hardware platform (for example “i686” or “Power Macintosh”).</td>
</tr>
<tr class="field"><th class="field-name"><cite>processor</cite>:</th><td class="field-body">The processor architecture (for example “athlon” or “powerpc”).</td>
</tr>
<tr class="field"><th class="field-name"><cite>name</cite>:</th><td class="field-body">The name of the slave.</td>
</tr>
<tr class="field"><th class="field-name"><cite>ipnr</cite>:</th><td class="field-body">The IP address of the slave.</td>
</tr>
</tbody>
</table>
<p>Note that not all of these properties may be available for all platforms,
depending on OS and Python version.</p>
<div class="section" id="examples">
<h2>2.1   Examples</h2>
<p>To set up a target platform, create rules that are checked against the
properties of the slave. For example, a target platform that matches slave
running Linux on x86 would look like this:</p>
<table border="1" class="docutils">
<colgroup>
<col width="25%">
<col width="75%">
</colgroup>
<thead valign="bottom">
<tr><th class="head" rowspan="2">Property</th>
<th class="head" rowspan="2">Expression</th>
</tr>
<tr></tr>
</thead>
<tbody valign="top">
<tr><td><cite>os</cite></td>
<td><cite>^Linux</cite></td>
</tr>
<tr><td><cite>machine</cite></td>
<td><cite>^[xi]d?86$</cite></td>
</tr>
</tbody>
</table>
<p>A target platform that matches any slaves running on Windows might look
like this:</p>
<table border="1" class="docutils">
<colgroup>
<col width="25%">
<col width="75%">
</colgroup>
<thead valign="bottom">
<tr><th class="head" rowspan="2">Property</th>
<th class="head" rowspan="2">Expression</th>
</tr>
<tr></tr>
</thead>
<tbody valign="top">
<tr><td><cite>family</cite></td>
<td><cite>^nt$</cite></td>
</tr>
</tbody>
</table>
<p>The build master will request a build from at most one slave for every
target platform. So, for example, if there are three slaves connected that
are matching 'NetBSD x86', only one of them will perform the build of a
specific revision. Slaves that match a particular target platform are
treated as if they were completely interchangable.</p>
<p>If a slave connects that doesn't match any of the configured target platforms,
the build master will reject its registration.</p>
</div>
</div>
<div class="section" id="slave-configuration">
<h1>3   Slave Configuration</h1>
<p>When a build slave registers with a build master, it sends information about
the machine the slave is running on, and what software it has available.
While some of this information can be automatically discovered by the slave,
other information may need to be configured explicitly. Also, a slave instance
may want to override some of the automatically computed attributes,
for example to enable cross-compilation.</p>
<p>There are three categories of information that can be configured for a slave:</p>
<table class="docutils field-list" frame="void" rules="none">
<col class="field-name">
<col class="field-body">
<tbody valign="top">
<tr class="field"><th class="field-name"><cite>os</cite>:</th><td class="field-body">Properties of the operating system</td>
</tr>
<tr class="field"><th class="field-name"><cite>machine</cite>:</th><td class="field-body">Properties of the underlying hardware</td>
</tr>
<tr class="field"><th class="field-name"><cite>packages</cite>:</th><td class="field-body">Various pieces of software, like a language runtime or a library</td>
</tr>
</tbody>
</table>
<div class="section" id="configuration-file-format">
<h2>3.1   Configuration File Format</h2>
<p>For simple manual editing, the slave configuration file will be based on
the <tt class="docutils literal"><span class="pre">'INI'</span></tt> file format known from Windows, which is also frequently used by
Python applications.</p>
<p>The file is included at runtime using a slave command-line option:</p>
<pre class="literal-block">
bitten-slave -f config.ini
</pre>
<p>A configuration file is partitioned into named sections. There are two
predefined sections named <tt class="docutils literal"><span class="pre">[machine]</span></tt> and <tt class="docutils literal"><span class="pre">[os]</span></tt>. If you supply them in
your configuration file they should include the following sections.</p>
<div class="highlight"><pre><span class="k">[os]</span>
<span class="na">name</span> <span class="o">=</span> <span class="s">Darwin</span>
<span class="na">version</span> <span class="o">=</span> <span class="s">8.1.0</span>
<span class="na">family</span> <span class="o">=</span> <span class="s">posix</span>

<span class="k">[machine]</span>
<span class="na">name</span> <span class="o">=</span> <span class="s">levi</span>
<span class="na">processor</span> <span class="o">=</span> <span class="s">Power Macintosh</span>
</pre></div>
<p>There may be any number of additional sections, where each section corresponds
to a software package. For example:</p>
<div class="highlight"><pre><span class="k">[dbxml]</span>
<span class="na">version</span> <span class="o">=</span> <span class="s">2.1.8</span>

<span class="k">[python]</span>
<span class="na">version</span> <span class="o">=</span> <span class="s">2.3.5</span>
<span class="na">path</span> <span class="o">=</span> <span class="s">/usr/bin/python2.3</span>
</pre></div>
<p><em>Note:</em> Options called <tt class="docutils literal"><span class="pre">name</span></tt> is not allowed in custom sections (will
be skipped).</p>
<p>The build slave sends this package information as part of the build initiation,
which when using verbose logging (<tt class="docutils literal"><span class="pre">bitten-slave</span> <span class="pre">-v</span></tt>) will display a debug
message 'Sending slave configuration:' followed by:</p>
<div class="highlight"><pre><span class="nt">&lt;slave</span> <span class="na">name=</span><span class="s">"host.domain"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;platform</span> <span class="na">processor=</span><span class="s">"Power Macintosh"</span><span class="nt">&gt;</span>levi<span class="nt">&lt;/platform&gt;</span>
  <span class="nt">&lt;os</span> <span class="na">version=</span><span class="s">"8.1.0"</span> <span class="na">family=</span><span class="s">"posix"</span><span class="nt">&gt;</span>Darwin<span class="nt">&lt;/os&gt;</span>
  <span class="nt">&lt;package</span> <span class="na">name=</span><span class="s">"dbxml"</span> <span class="na">version=</span><span class="s">"2.1.8"</span> <span class="nt">/&gt;</span>
  <span class="nt">&lt;package</span> <span class="na">name=</span><span class="s">"python"</span> <span class="na">version=</span><span class="s">"2.3.5"</span> <span class="na">path=</span><span class="s">"/usr/bin/python23"</span> <span class="nt">/&gt;</span>
<span class="nt">&lt;/slave&gt;</span>
</pre></div>
<p>The name of the slave can only be set as command-line option:</p>
<pre class="literal-block">
bitten-slave --name=myhost
</pre>
</div>
<div class="section" id="commands-using-properties">
<h2>3.2   Commands using Properties</h2>
<p>A number of <a class="reference external" href="commands.html">commands</a> support runtime settings using a slave configuration
file. The example of <tt class="docutils literal"><span class="pre">python.path</span></tt> above is one such example, where all
Python commands will use the specified executable for running commands.</p>
<p>The documentation for <a class="reference external" href="commands.html">commands</a> should include information about all
runtime settings.</p>
</div>
<div class="section" id="properties-in-build-configurations">
<h2>3.3   Properties in Build Configurations</h2>
<p>Defined properties can be used in a build configuration to match slaves
against target platforms. For example, the following rule would match any slave
providing 'Berkeley DB XML' version 2.x:</p>
<pre class="literal-block">
dbxml.version ~= /^2\.\d\.\d.*/
</pre>
<p>The properties are accessible in dotted notation, where the part before the dot
is the package name, and the part after the dot is the name of the property.</p>
</div>
<div class="section" id="property-interpolation-in-build-recipes">
<h2>3.4   Property Interpolation in Build Recipes</h2>
<p>Property values can be interpolated into build <a class="reference external" href="recipes.html">recipes</a> as well, so individual
slaves can parameterize how their build is perfomed. For example, given the
following build recipe excerpt:</p>
<div class="highlight"><pre><span class="nt">&lt;svn:checkout</span> <span class="na">url=</span><span class="s">"http://svn.example.org/repos/myproject/"</span>
  <span class="na">path=</span><span class="s">"${repository.branch}"</span> <span class="na">revision=</span><span class="s">"${revision}"</span><span class="nt">/&gt;</span>
</pre></div>
<p>Slaves may control which part of the repository is checked out and tested
with a configuration file excerpt like this one:</p>
<div class="highlight"><pre><span class="k">[repository]</span>
<span class="na">branch</span> <span class="o">=</span> <span class="s">/branches/0.3-testing</span>
</pre></div>
<p>Default slave properties are also available for use in recipes:</p>
<div class="highlight"><pre><span class="nt">&lt;sh:exec</span> <span class="na">executable=</span><span class="s">"echo"</span>
  <span class="na">args=</span><span class="s">"Slave: ${family} ${os} ${version} ${machine} ${processor}"</span><span class="nt">/&gt;</span>
</pre></div>
<p>Additionally, environment variables are also interpolated, supporting
the common notations of <tt class="docutils literal"><span class="pre">$VAR</span></tt> and <tt class="docutils literal"><span class="pre">${VAR}</span></tt>.</p>
<div class="highlight"><pre><span class="nt">&lt;sh:exec</span> <span class="na">executable=</span><span class="s">"${PROGRAMFILES}/SomeDir/MyProg.exe"</span> <span class="nt">/&gt;</span>
</pre></div>
</div>
</div>
<div class="section" id="authentication">
<h1>4   Authentication</h1>
<p>Authentication information can also be included in slave configuration file:</p>
<div class="highlight"><pre><span class="k">[authentication]</span>
<span class="na">username</span> <span class="o">=</span> <span class="s">myusername</span>
<span class="na">password</span> <span class="o">=</span> <span class="s">mypassword</span>
</pre></div>
<p>The authentication information will be removed as soon as it is read
by the slave, and will not be passed to the master as active configuration.</p>
</div>
    <div id="footer">
      Visit the Bitten open source project at
      <a href="http://bitten.edgewall.org/">http://bitten.edgewall.org/</a>
    </div>
  </div>
</body>
</html>
