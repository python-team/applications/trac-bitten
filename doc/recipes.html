<!DOCTYPE html>

<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="generator" content="Docutils 0.5: http://docutils.sourceforge.net/">
<title>Bitten: Build Recipes</title>
<link rel="stylesheet" href="common/style/edgewall.css" type="text/css">
</head>
<body>
<div class="document" id="build-recipes">
    <div id="navigation">
      <span class="projinfo">Bitten 0.6</span>
      <a href="index.html">Documentation Index</a>
    </div>
<h1 class="title">Build Recipes</h1>
<p>A build recipe tells a build slave how a project is to be built. It consists of
multiple build steps, each defining a command to execute, and where artifacts
can be found after that command has successfully completed.</p>
<p>Build recipes are intended to supplement existing project build files (such as
Makefiles), not to replace them. In general, a recipe will be much simpler than
the build file itself, because it doesn't deal with all the details of the
build. It just automates the execution of the build and lets the build slave
locate any artifacts and metrics data generated in the course of the build.</p>
<p>A recipe can and should split the build into multiple separate steps so that the
build slave can provide better status reporting to the build master while the
build is still in progress. This is important for builds that might take long to
execute. In addition, build steps help organize the build results for a more
structured presentation.</p>
<div class="section" id="file-format">
<h1>File Format</h1>
<p>Build recipes are stored internally in an XML-based format. Recipe documents
have a single <tt class="docutils literal"><span class="pre">&lt;build&gt;</span></tt> root element with one or more <tt class="docutils literal"><span class="pre">&lt;step&gt;</span></tt> child
elements. The steps are executed in the order they appear in the recipe.</p>
<p>A <tt class="docutils literal"><span class="pre">&lt;step&gt;</span></tt> element will consist of any number of commands and reports. Most of
these elements are declared in XML namespaces, where the namespace URI defines
a collection of related commands.</p>
<p>The <tt class="docutils literal"><span class="pre">&lt;build&gt;</span></tt> element can optionally have an <tt class="docutils literal"><span class="pre">onerror</span></tt> attribute that
dictates how a build should proceed after the failure of a step. Allowable
values are:</p>
<ul class="simple">
<li><tt class="docutils literal"><span class="pre">fail</span></tt>: failure of a step causes the build to terminate. (default)</li>
<li><tt class="docutils literal"><span class="pre">continue</span></tt>: builds continue after step failures. Failing steps
contribute to the overall build status.</li>
<li><tt class="docutils literal"><span class="pre">ignore</span></tt>: builds continue after step failures. Builds are marked
as successful even in the presence of failed steps with
onerror='ignore'</li>
</ul>
<p><tt class="docutils literal"><span class="pre">&lt;step&gt;</span></tt> elements can override the <tt class="docutils literal"><span class="pre">&lt;build&gt;</span></tt> <tt class="docutils literal"><span class="pre">onerror</span></tt> attribute with
their own <tt class="docutils literal"><span class="pre">onerror</span></tt> attributes.</p>
<p>Commonly, the first step of any build recipe will perform the checkout from the
repository.</p>
<div class="highlight"><pre><span class="nt">&lt;build</span> <span class="na">xmlns:python=</span><span class="s">"http://bitten.edgewall.org/tools/python"</span>
       <span class="na">xmlns:svn=</span><span class="s">"http://bitten.edgewall.org/tools/svn"</span><span class="nt">&gt;</span>

  <span class="nt">&lt;step</span> <span class="na">id=</span><span class="s">"checkout"</span> <span class="na">description=</span><span class="s">"Checkout source from repository"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;svn:checkout</span> <span class="na">url=</span><span class="s">"http://svn.example.org/repos/foo"</span>
        <span class="na">path=</span><span class="s">"${path}"</span> <span class="na">revision=</span><span class="s">"${revision}"</span> <span class="nt">/&gt;</span>
  <span class="nt">&lt;/step&gt;</span>

  <span class="nt">&lt;step</span> <span class="na">id=</span><span class="s">"build"</span> <span class="na">description=</span><span class="s">"Compile to byte code"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;python:distutils</span> <span class="na">command=</span><span class="s">"build"</span><span class="nt">/&gt;</span>
  <span class="nt">&lt;/step&gt;</span>

  <span class="nt">&lt;step</span> <span class="na">id=</span><span class="s">"test"</span> <span class="na">description=</span><span class="s">"Run unit tests"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;python:distutils</span> <span class="na">command=</span><span class="s">"unittest"</span><span class="nt">/&gt;</span>
    <span class="nt">&lt;python:unittest</span> <span class="na">file=</span><span class="s">"build/test-results.xml"</span><span class="nt">/&gt;</span>
    <span class="nt">&lt;python:trace</span> <span class="na">summary=</span><span class="s">"build/test-coverage.txt"</span>
        <span class="na">coverdir=</span><span class="s">"build/coverage"</span> <span class="na">include=</span><span class="s">"trac*"</span> <span class="na">exclude=</span><span class="s">"*.tests.*"</span><span class="nt">/&gt;</span>
  <span class="nt">&lt;/step&gt;</span>

<span class="nt">&lt;/build&gt;</span>
</pre></div>
<p>See <a class="reference external" href="commands.html">Build Recipe Commands</a> for a comprehensive reference of the commands
available by default.</p>
<p>Recipes may contain variables, for example <tt class="docutils literal"><span class="pre">${path}</span></tt>, which are expanded
before the recipe is executed. A small set of variables is pre-defined
but custom variables may be added (see <a class="reference external" href="configure.html">Slave Configuration</a> for further
instructions). The pre-defined recipe variables are:</p>
<table border="1" class="docutils">
<colgroup>
<col width="23%">
<col width="77%">
</colgroup>
<thead valign="bottom">
<tr><th class="head">Variable name</th>
<th class="head">Expanded value</th>
</tr>
</thead>
<tbody valign="top">
<tr><td><tt class="docutils literal"><span class="pre">${path}</span></tt></td>
<td>Repository path from the build configuration</td>
</tr>
<tr><td><tt class="docutils literal"><span class="pre">${config}</span></tt></td>
<td>The build configuration name</td>
</tr>
<tr><td><tt class="docutils literal"><span class="pre">${build}</span></tt></td>
<td>The index of this build request</td>
</tr>
<tr><td><tt class="docutils literal"><span class="pre">${revision}</span></tt></td>
<td>The repository revision being tested</td>
</tr>
<tr><td><tt class="docutils literal"><span class="pre">${platform}</span></tt></td>
<td>The name of the target platform being built</td>
</tr>
<tr><td><tt class="docutils literal"><span class="pre">${name}</span></tt></td>
<td>The name of the build slave</td>
</tr>
<tr><td><tt class="docutils literal"><span class="pre">${basedir}</span></tt></td>
<td>The absolute path of the build location, joining
<tt class="docutils literal"><span class="pre">work-dir</span></tt> (absolute) with <tt class="docutils literal"><span class="pre">build-dir</span></tt> (relative)</td>
</tr>
</tbody>
</table>
<p>As the recipe needs to be valid XML, any reserved characters in attributes must
be quoted using regular XML entities:</p>
<table border="1" class="docutils">
<colgroup>
<col width="48%">
<col width="52%">
</colgroup>
<thead valign="bottom">
<tr><th class="head">Character</th>
<th class="head">Quoted</th>
</tr>
</thead>
<tbody valign="top">
<tr><td><tt class="docutils literal"><span class="pre">"</span></tt></td>
<td><tt class="docutils literal"><span class="pre">&amp;quot;</span></tt></td>
</tr>
<tr><td><tt class="docutils literal"><span class="pre">&lt;</span></tt></td>
<td><tt class="docutils literal"><span class="pre">&amp;lt;</span></tt></td>
</tr>
<tr><td><tt class="docutils literal"><span class="pre">&gt;</span></tt></td>
<td><tt class="docutils literal"><span class="pre">&amp;gt;</span></tt></td>
</tr>
<tr><td><tt class="docutils literal"><span class="pre">&amp;</span></tt></td>
<td><tt class="docutils literal"><span class="pre">&amp;amp;</span></tt></td>
</tr>
<tr><td><tt class="docutils literal"><span class="pre">'</span></tt></td>
<td><tt class="docutils literal"><span class="pre">&amp;apos;</span></tt></td>
</tr>
</tbody>
</table>
<p>If needed, most commands use regular shell rules to split parts of the input -
typically like <tt class="docutils literal"><span class="pre">args</span></tt> input for <tt class="docutils literal"><span class="pre">sh:exec</span></tt> command. Double-quotes
(<tt class="docutils literal"><span class="pre">&amp;quot;</span></tt>) can be used to mark the start and end if any sub-parts contain
whitespace, alternatively <tt class="docutils literal"><span class="pre">'\'</span></tt> can be used to escape whitespace or any other
character that carries meaning as part of input - including double-quotes and
backslash itself:</p>
<div class="highlight"><pre><span class="nt">&lt;sh:exec</span> <span class="na">file=</span><span class="s">"echo"</span> <span class="na">args=</span><span class="s">"o\\ne &amp;quot;4 2&amp;quot; \&amp;quot;hi\ there\&amp;quot;"</span><span class="nt">/&gt;</span>
</pre></div>
<p>This will pass 3 arguments: <tt class="docutils literal"><span class="pre">o\ne</span></tt> + <tt class="docutils literal"><span class="pre">4</span> <span class="pre">2</span></tt> + <tt class="docutils literal"><span class="pre">"hi</span> <span class="pre">there"</span></tt>.</p>
<p><strong>Note:</strong> On Windows, batch scripts and built-ins will execute through a shell.
This may affect quoting of arguments.</p>
</div>
    <div id="footer">
      Visit the Bitten open source project at
      <a href="http://bitten.edgewall.org/">http://bitten.edgewall.org/</a>
    </div>
  </div>
</body>
</html>
